﻿using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System.Linq;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.OutputEncoding = Encoding.UTF8;
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            var user = new { Id = 1, AccountId = 1, Login = "snow", Password = "111" };
            var sumN = 10100;

            System.Console.WriteLine($"Аккаунт пользователя {user.Id}: {          atmManager.GetUserAccount(user.AccountId, user.Login, user.Password)}");
            System.Console.WriteLine($"Все аккаунты пользователя {user.Id}: {     atmManager.GetAllAccounts(user.Id)}");
            System.Console.WriteLine($"История аккаунтов пользователя {user.Id}: {atmManager.GetAllAccountsHistory(user.Id)}");
            System.Console.WriteLine($"Все пополнения: {                          atmManager.GetAllInputs()}");
            System.Console.WriteLine($"Все пользователи с суммой больше {sumN}: { atmManager.GetUsersWithSumMoreN(sumN)}");

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}