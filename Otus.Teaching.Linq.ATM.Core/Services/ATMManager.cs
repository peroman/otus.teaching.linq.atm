﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public string GetUserAccount(int accountId, string login, string password)
        {
            var userAccount = (from user in Users
                               join account in Accounts on user.Id equals account.UserId
                               where account.Id == accountId && user.Login == login && user.Password == password
                               select new
                               {
                                   AccountId = account.Id,
                                   Fio = $"{user.SurName} {user.FirstName} {user.MiddleName}",
                                   account.OpeningDate,
                                   account.CashAll
                               }).FirstOrDefault();

            var jsonString = JsonSerializer.Serialize(userAccount, new JsonSerializerOptions() { WriteIndented = true });

            return jsonString;
        }

        public string GetAllAccounts(int userId)
        {
            var accounts = from account in Accounts
                           where account.UserId == userId
                           orderby account.OpeningDate
                           select new
                           {
                               AccountId = account.Id,
                               account.OpeningDate,
                               account.CashAll
                           };

            var jsonString = JsonSerializer.Serialize(accounts, new JsonSerializerOptions() { WriteIndented = true });

            return jsonString;
        }

        public string GetAllAccountsHistory(int userId)
        {
            var accounts = from account in Accounts
                           join history in History on account.Id equals history.AccountId
                           where account.UserId == userId
                           group new { account, history } by account.Id into g
                           select new
                           {
                               AccountId = g.Key,
                               OpenningDate = g.First().account.OpeningDate,
                               History = from t in g select t.history
                           };

            var jsonString = JsonSerializer.Serialize(accounts, new JsonSerializerOptions() { WriteIndented = true });

            return jsonString;
        }

        public string GetAllInputs()
        {
            var accounts = from user in Users
                           join account in Accounts on user.Id equals account.UserId
                           join history in History on account.Id equals history.AccountId
                           where history.OperationType == OperationType.InputCash
                           orderby user.SurName, user.FirstName, user.MiddleName, user.Id, history.OperationDate descending
                           select new
                           {
                               AccountId = account.Id,
                               Fio = $"{user.SurName} {user.FirstName} {user.MiddleName}",
                               history.OperationDate,
                               history.CashSum
                           };

            var jsonString = JsonSerializer.Serialize(accounts, new JsonSerializerOptions() { WriteIndented = true });

            return jsonString;
        }

        public string GetUsersWithSumMoreN(object oSumN)
        {
            if (!decimal.TryParse(oSumN.ToString(), out var sumN))
            {
                return "Ошибка! Задано некоректное значение.";
            }

            var accounts = from user in Users
                           join account in Accounts on user.Id equals account.UserId
                           where account.CashAll > sumN
                           group new { user, account } by user.Id into g
                           select new
                           {
                               UserId = g.FirstOrDefault().user.Id,
                               Fio = $"{g.FirstOrDefault().user.SurName} {g.First().user.FirstName} {g.First().user.MiddleName}",
                               AccountId = g.FirstOrDefault().account.Id,
                               g.FirstOrDefault().account.OpeningDate,
                               g.FirstOrDefault().account.CashAll,
                           };

            var jsonString = JsonSerializer.Serialize(accounts, new JsonSerializerOptions() { WriteIndented = true });

            return jsonString;
        }
    }
}